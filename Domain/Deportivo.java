package Domain;

public class Deportivo extends Zapato{

    private String deporte;
    
    //A la hora de usar super, este tiene que hacer referencia a un constructor de la clase padre, en este caso al
    //no recibir parametros hace referencia al constructor de la clase padre que no recibe parametros
    public Deportivo() {
        super();
    }

    //En este otro caso como super recibe parametros, hace referencia al constructor de la clase que recibe parametros
    //el orden y el tipo de parametros influyen
    //Si se ingresa por ejemplo un super(int, String) hara referencia a un constructor de la clase padre que reciba por parametros
    //un int y String en ese orden
    //En caso de que se ingrese un super(String, int) no hara referencia al mismo constructor ya que el orden de las variables no
    //es el mismo
    public Deportivo(String deporte, int numCalzado) {
        super(numCalzado);
        this.deporte = deporte;
    }

    public Deportivo(String deporte) {
        this.deporte = deporte;
    }

    //Para llamar un metodo o atributo de la clase padre desde la clase hija se utiliza super.nombreMetodo()
    @Override
    public String toString() {
        return super.toString() + "Deportivo{" + "deporte=" + deporte + '}';
    }
}