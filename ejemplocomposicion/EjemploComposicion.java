package ejemplocomposicion;

import Domain.Deportivo;
import Domain.Organo;
import Domain.Sistema;
import Domain.Zapato;
import java.util.ArrayList;

public class EjemploComposicion {

    public static void main(String[] args) {
        //COMPOSICION
        //Creamos nuestro sistema que va a tener una lista de organos
        Sistema digestivo = new Sistema();
        
        //Creamos los organos que van a estar contenidos en ese sistema
        Organo estomago = new Organo("Estomago", "Digerir alimetos");
        Organo boca = new Organo("Boca", "Masticar");
        
        //Para trabajarlos mejor y de manera dinamica, los agregamos a un ArrayList que va a ser un atributo del sistema
        ArrayList<Organo> organos = new ArrayList<Organo>();
        organos.add(boca);
        organos.add(estomago);
        
        //Le insertamos como atributo la lista de organos que creamos anteriormente
        digestivo.setOrganos(organos);
        digestivo.setNombre("Digestivo");
        
        //Mostramos el ejemplo
        //Notese que no llamamos a los organos en si, sino a su contenedor que nos brinda sus datos
        digestivo.imprimir();
        
        //HERENCIA
        //Creamos un zapato de tipo deportivo, el objeto deportivo tiene los atributos de un zapato
        //Por ende decimos que Zapato es la clase Padre que pasa sus atributos a la clase Hija, en este caso un zapato Deportivo
        Zapato nike = new Deportivo("Futbol", 38);
        Deportivo adidas = new Deportivo("Futbol",40);
        
        //Mostramos el ejemplo
        //Notese que al llamar a su metodo toString no importa como lo declaremos, siempre se pueden obtener los
        //atributos de la clase padre sin hacer referencia directamente
        System.out.println(nike.toString());
        System.out.println(adidas.toString());
    }
}